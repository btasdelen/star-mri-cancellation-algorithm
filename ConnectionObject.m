classdef ConnectionObject
    %CONNECTIONOBJECT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        raspi
        meas_dev
        rpi_ip
        meas_dev_ip
        meas_dev_type
        num_ch
    end
    
    properties(Constant)
        NAV_CONT = 1
        NAV_STOP = 0
    end
    
    methods
        function obj = ConnectionObject(varargin)
            %CONNECTIONOBJECT Construct an instance of this class
            %   Detailed explanation goes here
            % only want 3 optional inputs at most
            numvarargs = length(varargin);
            if numvarargs > 3
                error('ConnectionObject:TooManyInputs', ...
                    'requires at most 3 optional inputs');
            end

            % set defaults for optional inputs
            optargs = {'169.254.39.10'  '192.168.2.3' 'MR'};

            % now put these defaults into the valuesToUse cell array, 
            % and overwrite the ones specified in varargin.
            optargs(1:numvarargs) = varargin;
            % or ...
            % [optargs{1:numvarargs}] = varargin{:};

            % Place optional args in memorable variable names
            [rpi_ip, meas_dev_ip, meas_dev_type] = optargs{:};
            
            instrreset
            
            obj.rpi_ip = rpi_ip;
            obj.meas_dev_ip = meas_dev_ip;
            obj.meas_dev_type = meas_dev_type;
            
            obj.raspi = tcpip(rpi_ip, 8101, 'NetworkRole', 'client');
            
            switch meas_dev_type
                case 'MR'
                    obj.meas_dev = tcpip(meas_dev_ip, 8101, ...
                        'NetworkRole', 'server', 'Timeout', 25);
                    obj.meas_dev.InputBufferSize = 512;
                    obj.meas_dev.ByteOrder = 'littleEndian';                   
                case 'NA'
                    obj.meas_dev = tcpip(meas_dev_ip, 5025, 'NetworkRole', 'client');
            end
            obj.num_ch = 1;
        end
        
        function ConnectRaspberryPi(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            fopen(obj.raspi);
            disp(['Connected to Raspberry Pi on ip: ' obj.rpi_ip])
        end
        
        function ConnectMeasDev(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            fopen(obj.meas_dev);
            disp(['Connected to Measurement Device ' obj.meas_dev_type ' on ip: ' obj.meas_dev_ip])
            
            if strcmp(obj.meas_dev_type, 'NA')
                fprintf(obj.meas_dev, "SENS:FREQ:CENTER 123.237E6");
                fprintf(obj.meas_dev, "SENS:FREQ:SPAN 1");
                fprintf(obj.meas_dev, "CALC1:PAR:DEF S21");
                fprintf(obj.meas_dev, "CALC1:FORM POL");               
            end
        end
        
        function [m, meas_sample_size] = QueryMeasurement(obj)
            %UNTITLED Summary of this function goes here
            %   Detailed explanation goes here
            
            switch obj.meas_dev_type
                case 'MR'
                    m = fread(obj.meas_dev, 2, 'float32')';
                    meas_sample_size = fread(obj.meas_dev, 1, 'uint32')';
                    m = m(1) + 1i*m(2);          
                case 'NA'
                    fprintf(obj.meas_dev, "CALC1:MARK1:Y?");
                    meas_str = fscanf(obj.meas_dev);
                    m_i = str2num(meas_str);
%                     [m_i(1), m_i(2)] = pol2cart(deg2rad(m_i(2)), m_i(1));
                    m = m_i(1) + 1i*m_i(2);
                    meas_sample_size = 1;
            end
        end

        function ok_msg = SetAttenuators(obj, code)
            fwrite(obj.raspi, ['SETATT:' num2str(code', '%d,')]);
            ok_msg = fread(obj.raspi,2);
            pause(0.02);
        end

        function G = CalculateScaling(obj, ScalerDiff)
            % Set and measure s(0,0,0,0)
            obj.SetAttenuators([0;0;0;0]);

            [m1, ~] = obj.QueryMeasurement();
            if obj.num_ch == 2
                [m21, ~] = obj.QueryMeasurement();
            end

            obj.SendContinueFeedback();

            % Measure s(127,127,127,127)
            obj.SetAttenuators([127;127;127;127]);

            obj.SendContinueFeedback();
            [m0, ~]  = obj.QueryMeasurement();

            if obj.num_ch == 2
                [m20, ~] = obj.QueryMeasurement();
            end

            G = (m1 - m0)/ScalerDiff;
        end
        
        function DisconnectRaspberryPi(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            fwrite(obj.raspi, 'EX');
            fclose(obj.raspi);
            disp(['Disconnected from Raspberry Pi on ip: ' obj.rpi_ip])
        end
        
        function DisconnectMeasDev(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            fclose(obj.meas_dev);
            disp(['Disconnected from Measurement Device ' obj.meas_dev_type ' on ip: ' obj.meas_dev_ip])
        end
        
        function SendContinueFeedback(obj)
            fwrite(obj.meas_dev, obj.NAV_CONT, 'uint32');
        end
        
        function StopFeedback(obj)
            fwrite(obj.meas_dev, obj.NAV_STOP, 'uint32');
        end
    end
end

