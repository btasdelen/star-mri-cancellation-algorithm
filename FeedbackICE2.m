%% Experiment Parameters
stop_threshold  = -85;     %dB
line_number     = 1;        % Number of feedback lines
max_iteration   = 300;
ch2             = true;

%% Initialize connections and experiment variables
if exist('lut', 'var') ~= 1
    LoadLUT;
end

conn_obj = ConnectionObject();
conn_obj.num_ch = 2;
conn_obj.ConnectRaspberryPi();
conn_obj.SetAttenuators([0;0;0;0]);

conn_obj.ConnectMeasDev();

[stime, m, m2, s_, l_] = deal(zeros(max_iteration, 1));

%% Get 2 initial measurements to find scaling G:
G1 = conn_obj.CalculateScaling(lut.ScalerDiff);
G = conn_obj.CalculateScaling(lut.ScalerDiff);

disp('Scaling calculated, proceed to decoupling loop...')
%% Reset state and initiate iterations
curr_state = ones(4,1)*127;
conn_obj.SetAttenuators(curr_state);
conn_obj.SendContinueFeedback();

tic
for line=1:line_number
    for k=1:max_iteration

        m_rcv = conn_obj.QueryMeasurement();
        m(k) = m_rcv/G;

        if ch2
            m_rcv2 = conn_obj.QueryMeasurement();
            m2(k) = m_rcv/G;
        end

        stime(k) = toc;

%         [curr_state, l_(k), s_(k)] = trivial_cancellation(m(k), curr_state, lut);
         [curr_state, l_(k), s_(k)] = no_search_cancellation(m(k), curr_state, lut);

        if mag2db(abs(m(k))) < stop_threshold 
            break;
        end
        
        conn_obj.SetAttenuators(curr_state);
        pause(0.01);
        conn_obj.SendContinueFeedback();

    end
    conn_obj.StopFeedback();
    disp(['Part ' num2str(line) ', iteration ' num2str(k) ...
        ', final decoupling ' num2str(mag2db(abs(m(k))))])
end
toc

%% Disconnect and cleanup
conn_obj.DisconnectMeasDev();
conn_obj.DisconnectRaspberryPi();
clear conn_obj

%% Plot 
stime = stime - stime(1);

figure; plot(stime(1:k), mag2db(abs(m(1:k))));
ylabel('Amplitude (dB)'); xlabel('Time (s)')
title('Coupled Signal');

figure;
subplot(1,2,1); plot(stime(1:k), mag2db(abs(l_(1:k))));
ylabel('Amplitude (dB)'); xlabel('Time (s)')
title('Estimated leak signal magnitude');

subplot(1,2,2); plot(stime(1:k), rad2deg(angle(l_(1:k))));
ylabel('Phase (deg)'); xlabel('Time (s)')
title('Estimated leak signal phase');

% figure; plot(stime, abs(s_));
% title('signal from circuit mag');
% figure; plot(stime, rad2deg(angle(s_)));
% title('signal from circuit pha');
% figure; plot(stime(2:end), abs(l_(1:end-1) + s_(2:end)));
% title('diff leak and s mag');
