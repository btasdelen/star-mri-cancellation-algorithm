tic

lut.MeasID = '190904'
% pcbmeasfile = sprintf('PCBMeasurements/Meas%s/avg%s.mat', lut.MeasID, lut.MeasID);
% load(pcbmeasfile)

fileName = ['lut' lut.MeasID '.h5f'];
%fileName = ['/home/bilal/lut' MeasID '.h5f'];

lut.s = h5read(fileName,'/lut');
lut.map = h5read(fileName,'/lut_map');
%phase_slice_starts = h5read(fileName,'/phase_slice_starts');
lut.region_boundaries = h5read(fileName,'/region_boundaries')';

lut.bias = h5read(fileName,'/bias')';
lut.minAmp = h5read(fileName,'/minAmp');
lut.maxAmp = h5read(fileName,'/maxAmp');
lut.dA = h5read(fileName,'/dA');
lut.dDeg = h5read(fileName,'/dDeg');
lut.ScalerDiff = h5read(fileName, '/ScalerDiff');
lut.ScalerDiff = lut.ScalerDiff(1) + 1i*lut.ScalerDiff(2);
S21 = h5read(fileName, '/S21');
S21 = permute(S21, [3 2 1]);
% Convert polar measurement matrix to complex matrix for convenience.
amps = zeros(128,4,2);
[amps(:,:,1), amps(:,:,2)] = pol2cart(deg2rad(S21(:, :,  2)), db2mag(S21(:, :, 1)));

lut.bias = lut.bias(1) + 1i*lut.bias(2);
lut.meas = amps(:,:,1) + 1i*amps(:,:,2);

clear amps S21 fileName
toc
