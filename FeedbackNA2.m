stop_threshold = -85; %dB

% Initialize connections and experiment variables
conn_obj = ConnectionObject('139.179.121.23', '169.254.119.201', 'NA');

conn_obj.ConnectRaspberryPi();
conn_obj.SetAttenuators([0;0;0;0]);

conn_obj.ConnectMeasDev();

iterNr = 100;

%% Initialize connections and experiment variables
if exist('lut', 'var') ~= 1
    LoadLUT;
end

pause_val = 0.25;
stime = zeros(iterNr, 1);
m = zeros(iterNr, 1);
s_ = zeros(iterNr, 1);
l_ = zeros(iterNr, 1);

%% Get 2 initial measurements to find scaling G:
G = conn_obj.CalculateScaling(lut.ScalerDiff);
if G == 0
    G=1;
end
%% Continue with iterations.
curr_state = ones(4,1)*127;
conn_obj.SetAttenuators(curr_state);
pause(0.01)

tic
for k=1:iterNr

    m_rcv = conn_obj.QueryMeasurement();
    m(k) = m_rcv/G;
    
    stime(k) = toc;

    [curr_state, l_(k), s_(k)] = no_search_cancellation(m(k), curr_state, lut);

    if mag2db(abs(m(k))) < stop_threshold
        pause(pause_val);
        break;
        continue;
    end

    conn_obj.SetAttenuators(curr_state);
    pause(pause_val);

end
toc

% Disconnect and cleanup
conn_obj.DisconnectRaspberryPi();

clear conn_obj

stime = stime - stime(1);

%% Plot results
figure; plot(stime(1:k), mag2db(abs(m(1:k))));
ylabel('Amplitude (dB)'); xlabel('Time (s)')
title('Coupled Signal');

figure;
subplot(1,2,1); plot(stime(1:k), mag2db(abs(l_(1:k))));
ylabel('Amplitude (dB)'); xlabel('Time (s)')
title('Estimated leak signal magnitude');

subplot(1,2,2); plot(stime(1:k), rad2deg(angle(l_(1:k))));
ylabel('Phase (deg)'); xlabel('Time (s)')
title('Estimated leak signal phase');

% figure; plot(stime, abs(s_));
% title('signal from circuit mag');
% figure; plot(stime, rad2deg(angle(s_)));
% title('signal from circuit pha');
% figure; plot(stime(2:end), abs(l_(1:end-1) + s_(2:end)));
% title('diff leak and s mag');
