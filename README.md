This repository contains Cancellation algorithm as proposed in:

Tasdelen B, Sadeghi-Tarakameh A, Yilmaz U, Atalar E. Dynamic Decoupling for Simultaneous Transmission and Acquisition in MRI. In: Proc. Intl. Soc. Mag. Reson. Med. 27. ; 2019. https://cds.ismrm.org/protected/19MPresentations/abstracts/4503.html

"synthesize_db.py" contains look-up table creation.

Other files in root directory implements cancellation loop and communication with ICE in Siemens Scanner.
