function plot_meas(data, line_no)
%PLOT_MEAS Plots amplitude and phase of PCB measurements.
  
    figure;
    title('Amplitude')
    for k=1:line_no
        subplot(2,2,k);
        plot(mag2db(abs(data(k,:))));
    end

    figure;
    title('Phase')
    for k=1:line_no
        subplot(2,2,k);
        plot(rad2deg(angle(data(k,:))));
    end

end

