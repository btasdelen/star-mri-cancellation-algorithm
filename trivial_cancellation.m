function [curr_state, l_, s_] = trivial_cancellation(m, prev_state, lut)
%TRIVIAL_CANCELLATION Summary of this function goes here
%   Detailed explanation goes here
    s_ = lut.meas(prev_state(1) + 1,1) + lut.meas(prev_state(2) + 1,2)...
        + lut.meas(prev_state(3) + 1,3) + lut.meas(prev_state(4) + 1,4) - lut.bias;
    l_ = m - s_;
    [cp, ca] = cart2pol(real(l_), imag(l_));
    cp = (rad2deg(cp) - 180*sign(cp));

    %% Jump to corresponding slice
    np = floor((cp + 180)/lut.dDeg) + 1;
    nd = floor(ca/lut.dA) + 1;
    idx_start = lut.region_boundaries(np,nd);
    idx_end   = lut.region_boundaries(np,nd+1);
    num_points = idx_end - idx_start;
    %% Set newly found attenuations
    idx = idx_start - 1 + binarySearch(lut.s(1,idx_start:idx_end), ca);
    curr_state = lut.map(:, idx);

end

