import numpy as np
from numpy.matlib import repmat
from scipy.io import loadmat
from mathutils import *
import itertools
import h5py

ext = 'mat'

exp_id = '190904'

dDeg = 0.5
dA = 1e-5

if ext == 'mat':
    mats = loadmat('../avg' + exp_id + '.' + ext)
elif ext == 'npz':
    mats = np.load('../avg' + exp_id + '.' + ext)

S21_avg = mats['S21_avg']
s0 = squeeze(mats['S0_avg'])

s0_cart = pol2cart(db2mag(s0[0]), np.deg2rad(s0[1]))

amps = np.zeros((2, 128, 4))
(amps[0,:,:], amps[1,:,:]) = pol2cart(db2mag(S21_avg[:,:,0]), np.deg2rad(S21_avg[:,:,1]))


s127_cart = np.sum(amps[:,127,:], axis=1)/4

ScalerDiff = s0_cart - s127_cart

print("Proceeding to create constellation.")
#  calculate joint possibilities
newDB = np.zeros((2, 128**4), dtype=np.float32)

d = np.arange(0, 128, 1)
first_coef = 128**3
second_coef = 128**2

for a, b, c in itertools.product(range(128), repeat=3):
    xy = amps[:, a, 0] + amps[:, b, 1] + amps[:, c, 2] + amps[:, :, 3].T
    idx = a*first_coef + b*second_coef + c*128 + d
    # Save as cartesian coordinates.
    newDB[:, idx] = xy[:,:].T 
    #(newDB[idx, 0], newDB[idx, 1]) = cart2pol(xy[:, 0] - mean_x, xy[:, 1] - mean_y)

mean_val = np.mean(newDB, axis=1)

print("Mean Value of constellation: " + str(mean_val))

newDB[:, :] = cart2pol(newDB[0, :] - mean_val[0], newDB[1, :] - mean_val[1])

#  calculate corresponding attenuations
print("Calculating codes.")

atts = np.zeros((128**4,4), dtype=np.uint8)
atts[:, 3] = repmat(np.arange(128, dtype=np.uint8), 1, first_coef)
atts[:, 2] = repmat(np.repeat(np.arange(128, dtype=np.uint8), 128), 1, second_coef)
atts[:, 1] = repmat(np.repeat(np.arange(128, dtype=np.uint8), second_coef), 1, 128)
atts[:, 0] = np.repeat(np.arange(128, dtype=np.uint8) ,first_coef)


#with open('synthDB.bin', 'wb') as ff:
#    newDB.tofile(ff)

#with open('synthDBAtts.bin', 'wb') as ff:
#    atts.tofile(ff)

print("Sorting for phases.")
#  Sort for Phase
I = np.argsort(newDB[1, :])
phaseSortedDB = (newDB[:, I]).T
phaseSortedatts = atts[I,:]


# Slice phases

phis = np.arange(-np.pi, np.pi + np.deg2rad(dDeg) , np.deg2rad(dDeg))

phase_slice_starts = np.zeros(phis.shape[0], dtype='intp')
print("Slicing starting...")
prev_idx = 0

phase_slice_starts[0] = 0;
phase_slice_starts[-1] = phaseSortedDB.shape[0];

for k in range(1, phase_slice_starts.shape[0] - 1):
    phase_slice_starts[k] = np.searchsorted(phaseSortedDB[prev_idx:, 1], phis[k], 'left').T + prev_idx
    prev_idx = phase_slice_starts[k]
    print(k, end=" ")


# Sort Amplitudes
I_last = np.zeros(phaseSortedDB.shape[0], dtype='intp')

for k in range(phase_slice_starts.shape[0]-1):
    interval = np.s_[phase_slice_starts[k]:phase_slice_starts[k+1]]
    I_last[interval] = np.argsort(phaseSortedDB[interval, 0])
    I_last[interval] = I_last[interval] + phase_slice_starts[k]
    print(k, end=":")
    print("" + str(interval.stop - interval.start) +  " at intervals " + str(interval.start) + " , " + str(interval.stop))

print("Indices found, now sorting...")
ampPhaSortedDB = phaseSortedDB[I_last, :]
ampPhaSortedAtts = phaseSortedatts[I_last, :]

# Slice Amplitudes
maxAmp = np.amax(ampPhaSortedDB[:, 0])
minAmp = np.amin(ampPhaSortedDB[:, 0])

ainterval = np.arange(0, maxAmp + dA, dA)

amp_slice_starts = np.zeros((phase_slice_starts.shape[0] - 1,ainterval.shape[0]), dtype='intp')
print("Slicing starting...")

for k in range(phase_slice_starts.shape[0]-1):
    interval = np.s_[phase_slice_starts[k]:phase_slice_starts[k+1]]
    amp_slice_starts[k, :] = np.searchsorted(ampPhaSortedDB[interval, 0], ainterval, 'left').T + interval.start + 1
    print(k, end=" ")


print("Saving data...")


with h5py.File('lut' + exp_id + '.h5f', 'w') as hf:
    hf.create_dataset("region_boundaries",   data=amp_slice_starts,     compression="gzip", compression_opts=2)
    hf.create_dataset("lut",                 data=ampPhaSortedDB,       compression="gzip", compression_opts=2)
    hf.create_dataset("lut_map",             data=ampPhaSortedAtts,     compression="gzip", compression_opts=2)
    hf.create_dataset("phase_slice_starts",  data=phase_slice_starts)
    hf.create_dataset("minAmp",              data=minAmp)
    hf.create_dataset("maxAmp",              data=maxAmp)
    hf.create_dataset("dA",                  data=dA)
    hf.create_dataset("dDeg",                data=dDeg)
    hf.create_dataset("bias",                data=mean_val)
    hf.create_dataset("ScalerDiff",          data=ScalerDiff)
    hf.create_dataset("S21",                 data=S21_avg)
