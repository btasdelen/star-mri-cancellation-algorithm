from itertools import tee
import operator
import numpy as np

def mag2db(x):
    return 20*np.log10(x)

def db2mag(x):
    return np.power(10, x/20)

def pol2cart(r, theta):
    cplx = r * np.exp( 1j * theta )
    return (np.real(cplx), np.imag(cplx))

def cart2pol(x, y):
    return (np.sqrt(x**2 + y**2), np.arctan2(y, x))

def is_sorted(iterable, compare=operator.le):
    a, b = tee(iterable)
    next(b, None)
    return all(map(compare, a, b))

def diffFlat(arr):
    darr = np.diff(arr)
    return darr.flatten()
