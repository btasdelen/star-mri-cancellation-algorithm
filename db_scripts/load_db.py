import numpy as np
import h5py

def load_db(filename):
    mat_dict = dict()
    with h5py.File(filename, 'r') as mats:
        for k,v in mats.items():
            mat_dict[k] = np.array(v)
    
    ampss2 = mat_dict['amp_slice_starts']
    ph_ss2 = mat_dict['phase_slice_starts']
    db2 = mat_dict['ampPhaSortedDB']
    att2 = mat_dict['ampPhaSortedAtts']
    return (ampss2, ph_ss2, db2, att2)

